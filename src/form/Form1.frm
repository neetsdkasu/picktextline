VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.OCX"
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "抜き出し"
   ClientHeight    =   3375
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   3375
   ScaleWidth      =   6015
   StartUpPosition =   3  'Windows の既定値
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   2400
      Top             =   3480
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   1800
      Top             =   3480
   End
   Begin MSComDlg.CommonDialog dlgOpen 
      Left            =   720
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      Filter          =   "テキストファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*"
   End
   Begin VB.Frame fraContainer 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6015
      Begin VB.CheckBox chkLike 
         Caption         =   "LIKE"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   2520
         Width           =   735
      End
      Begin VB.TextBox txtWord 
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   2880
         Width           =   3255
      End
      Begin VB.CommandButton cmdOutput 
         Caption         =   "Output"
         Height          =   375
         Left            =   3480
         TabIndex        =   5
         Top             =   2880
         Width           =   2415
      End
      Begin VB.ListBox List1 
         Enabled         =   0   'False
         Height          =   1860
         Index           =   1
         Left            =   120
         MultiSelect     =   2  '拡張
         OLEDropMode     =   1  '手動
         TabIndex        =   10
         Top             =   240
         Visible         =   0   'False
         Width           =   5775
      End
      Begin VB.CommandButton cmdDown 
         Caption         =   "Down"
         Height          =   255
         Left            =   2760
         TabIndex        =   9
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdUp 
         Caption         =   "Up"
         Height          =   255
         Left            =   2040
         TabIndex        =   8
         Top             =   2160
         Width           =   615
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   2160
         Width           =   855
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "Delete"
         Height          =   255
         Left            =   1080
         TabIndex        =   4
         Top             =   2160
         Width           =   855
      End
      Begin VB.CommandButton cmdAdd 
         Caption         =   "Add"
         Height          =   255
         Left            =   4680
         TabIndex        =   3
         Top             =   2160
         Width           =   1215
      End
      Begin VB.CommandButton cmdListChange 
         Caption         =   "Change"
         Height          =   255
         Left            =   3480
         TabIndex        =   2
         Top             =   2160
         Width           =   1095
      End
      Begin VB.ListBox List1 
         Height          =   1860
         Index           =   0
         Left            =   120
         MultiSelect     =   2  '拡張
         OLEDropMode     =   1  '手動
         TabIndex        =   1
         Top             =   240
         Width           =   5775
      End
      Begin VB.CheckBox chkCode 
         Caption         =   "Unicodeファイルとして処理"
         Height          =   300
         Left            =   960
         TabIndex        =   11
         Top             =   2520
         Width           =   2415
      End
      Begin VB.Label lblState 
         Alignment       =   2  '中央揃え
         Height          =   255
         Left            =   3480
         TabIndex        =   12
         Top             =   2520
         Width           =   2415
      End
   End
   Begin MSComDlg.CommonDialog dlgSave 
      Left            =   120
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "txt"
      Filter          =   "テキストファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*"
   End
   Begin VB.Menu mnuStop 
      Caption         =   "中止"
      Enabled         =   0   'False
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim mfsoFile   As Scripting.FileSystemObject
Dim mtxsOutput As Scripting.TextStream
Dim mtxsRead   As Scripting.TextStream
Dim mintCount  As Integer
Dim mintCurrent As Integer
Dim mblnUnicode As Boolean
Dim mblnLike As Boolean
Dim mlngLine As Long
Dim mstrWord As String

Private Sub ListAdd(strFile As String)
    On Error Resume Next
    
    Dim str1$, i&
    For i = 0 To List1(0).ListCount
        str1 = List1(0).List(i)
        If str1 = strFile Then
            Exit Sub
        End If
    Next i
    List1(0).AddItem strFile
    Dim fso1 As Scripting.FileSystemObject
    Set fso1 = New Scripting.FileSystemObject
    List1(1).AddItem fso1.GetFileName(strFile)
    Set fso1 = Nothing
End Sub

Private Sub ListSelectedCopy()
    On Error Resume Next
    
    Dim i&, p&
    p = IIf(List1(0).Visible, 0, 1)
    For i = 0 To List1(p).ListCount - 1
        List1(1 - p).Selected(i) = List1(p).Selected(i)
    Next i
End Sub

Private Sub cmdAdd_Click()
    On Error Resume Next
    
    dlgOpen.Flags = MSComDlg.FileOpenConstants.cdlOFNFileMustExist _
            Or MSComDlg.FileOpenConstants.cdlOFNPathMustExist _
            Or MSComDlg.FileOpenConstants.cdlOFNLongNames _
            Or MSComDlg.FileOpenConstants.cdlOFNHideReadOnly
    
    Err.Clear
    
    dlgOpen.ShowOpen
    
    If Err.Number = MSComDlg.ErrorConstants.cdlCancel Then
        Exit Sub
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Description, vbCritical Or vbOKOnly, Str(Err.Number)
        Exit Sub
    End If
    
    ListAdd dlgOpen.FileName
        
End Sub

Private Sub cmdClear_Click()
    On Error Resume Next
    
    If List1(0).ListCount = 0 Then Exit Sub
    If MsgBox("リストをクリアします", vbInformation Or vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    List1(0).Clear
    List1(1).Clear
End Sub

Private Sub cmdDelete_Click()
    On Error Resume Next
    
    If List1(0).ListCount = 0 Then Exit Sub
    If MsgBox("選択されている項目を削除します", vbInformation Or vbOKCancel, "確認") = vbCancel Then
        Exit Sub
    End If
    Dim i&, c&
    c = List1(0).ListCount
    ListSelectedCopy
    For i = c - 1 To 0 Step -1
        If List1(0).Selected(i) Then
            List1(0).RemoveItem i
            List1(1).RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdDown_Click()
    On Error Resume Next
    
    Dim i&, c&, str1$, lst1 As ListBox
    c = List1(0).ListCount
    If c < 2 Then Exit Sub
    ListSelectedCopy
    Dim k&
    k = IIf(List1(0).Selected(c - 1), c - 1, c)
    For i = c - 2 To 0 Step -1
        If List1(0).Selected(i) Then
            If k - i = 1 Then
                k = i
            Else
                For Each lst1 In List1
                    str1 = lst1.List(i)
                    lst1.RemoveItem i
                    lst1.AddItem str1, i + 1
                    lst1.Selected(i + 1) = True
                Next lst1
            End If
        End If
    Next i
End Sub

Private Sub cmdListChange_Click()
    On Error Resume Next
    
    Dim lst1 As ListBox
    ListSelectedCopy
    For Each lst1 In List1
        With lst1
            .Visible = Not (.Visible)
            .Enabled = Not (.Enabled)
        End With
    Next lst1
End Sub

Private Sub cmdOutput_Click()
    Dim strFile$
    
    mblnUnicode = (chkCode.Value = vbChecked)
    mblnLike = (chkLike.Value = vbChecked)
    
    mstrWord = txtWord.Text
    
    If Len(mstrWord) = 0 Then Exit Sub
    
    If List1(0).ListCount = 0 Then Exit Sub
    
    dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNHideReadOnly _
                Or MSComDlg.FileOpenConstants.cdlOFNPathMustExist _
                Or MSComDlg.FileOpenConstants.cdlOFNOverwritePrompt
    
    On Error GoTo ERROR_cmdOutput_Click
    
    Err.Clear
    
    dlgSave.ShowSave
    
    strFile = dlgSave.FileName
    
    fraContainer.Enabled = False
    Screen.MousePointer = vbHourglass
    lblState.Caption = "Wait..."
    Me.Caption = "抜き出し中..."
    mnuStop.Enabled = True
    
    Set mfsoFile = New Scripting.FileSystemObject
    Set mtxsOutput = mfsoFile.CreateTextFile(strFile, , mblnUnicode)
    
    mintCurrent = 0
    mintCount = List1(0).ListCount
    mlngLine = 0
    
    
    Timer1.Enabled = True
    
    Exit Sub
    
ERROR_cmdOutput_Click:
    If Err.Number <> MSComDlg.ErrorConstants.cdlCancel Then
        MsgBox Err.Description, vbCritical Or vbOKOnly, Str(Err.Number)
        Debug.Print "ERROR "; Err.Number, Err.Description
        Screen.MousePointer = vbDefault
    End If
End Sub


Private Sub cmdUp_Click()
    On Error Resume Next
    
    Dim i&, c&, str1$, lst1 As ListBox
    c = List1(0).ListCount
    If c < 2 Then Exit Sub
    ListSelectedCopy
    Dim k&
    k = IIf(List1(0).Selected(0), 0, -1)
    For i = 1 To c - 1
        If List1(0).Selected(i) Then
            If i - k = 1 Then
                k = i
            Else
                For Each lst1 In List1
                    str1 = lst1.List(i)
                    lst1.RemoveItem i
                    lst1.AddItem str1, i - 1
                    lst1.Selected(i - 1) = True
                Next lst1
            End If
        End If
    Next i
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    
    If fraContainer.Enabled = False Then
        If MsgBox("作業中ですが終了しますか？", vbInformation Or vbYesNo, "確認") = vbNo Then
            Cancel = 1
        End If
    End If
End Sub

Private Sub List1_OLEDragDrop(Index As Integer, Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error Resume Next
    
    If Data.GetFormat(vbCFFiles) = False Then
        Effect = vbDropEffectNone
        Exit Sub
    End If
    Dim str1
    For Each str1 In Data.Files
        ListAdd CStr(str1)
    Next str1
End Sub

Private Sub mnuStop_Click()
    On Error Resume Next
    
    If fraContainer.Enabled = False Then
        If MsgBox("作業を中止しますか？", vbInformation Or vbYesNo, "確認") = vbNo Then
            Exit Sub
        End If
        Timer1.Enabled = False
        Timer2.Enabled = False
        mtxsRead.Close
        mtxsOutput.Close
        fraContainer.Enabled = True
        lblState.Caption = "Complete"
        Set mfsoFile = Nothing
        Set mtxsOutput = Nothing
        Set mtxsRead = Nothing
        Screen.MousePointer = vbDefault
        Me.Caption = "抜き出し"
        mnuStop.Enabled = False
    End If
End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    
    If Timer1.Enabled = False Then Exit Sub
    Timer1.Enabled = False
    
    If mintCurrent = mintCount Then
    
        mtxsOutput.Close
        fraContainer.Enabled = True
        lblState.Caption = "Complete"
        Set mfsoFile = Nothing
        Set mtxsOutput = Nothing
        Set mtxsRead = Nothing
        Screen.MousePointer = vbDefault
        Me.Caption = "抜き出し"
        mnuStop.Enabled = False
        Exit Sub
    End If

    Dim i%
    For i = 0 To mintCount - 1
        List1(0).Selected(i) = (i = mintCurrent)
        List1(1).Selected(i) = (i = mintCurrent)
    Next i
    
    Set mtxsRead = mfsoFile.OpenTextFile( _
                    List1(0).List(mintCurrent), , , _
                        IIf(mblnUnicode, Scripting.Tristate.TristateTrue, _
                                        Scripting.Tristate.TristateFalse))

    mintCurrent = mintCurrent + 1
    Me.Caption = "抜き出し中..." & Str(mintCurrent) & "/" & Str(mintCount)
    
    Timer2.Enabled = True

End Sub

Private Sub Timer2_Timer()
    On Error Resume Next
    
    Static n As Long
    If Timer2.Enabled = False Then Exit Sub
    Timer2.Enabled = False
    
    If mtxsRead.AtEndOfStream Then
        n = 0
        mtxsRead.Close
        Timer1.Enabled = True
        Exit Sub
    End If
    
    Dim str1$
    
    str1 = mtxsRead.ReadLine
    
    If mblnLike Then
        If str1 Like mstrWord Then
            mtxsOutput.WriteLine str1
            mlngLine = mlngLine + 1
        End If
    Else
        If InStr(1, str1, mstrWord) > 0 Then
            mtxsOutput.WriteLine str1
            mlngLine = mlngLine + 1
        End If
    End If
    n = n + 1
    If n Mod 100 = 0 Then
        lblState.Caption = Str(mlngLine) & "/" _
                            & Str(n) & "/" _
                            & Str(mintCurrent) & "/" _
                            & Str(mintCount)
        lblState.Refresh
    End If

    Timer2.Enabled = True

End Sub
